Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex: