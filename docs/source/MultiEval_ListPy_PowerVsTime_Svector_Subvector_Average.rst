Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:SUBVector<SubVector>:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:SUBVector<SubVector>:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Subvector_.Average.Average
	:members:
	:undoc-members:
	:noindex: