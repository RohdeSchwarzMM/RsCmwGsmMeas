Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:GSM:MEASurement<Instance>:MEValuation:BER
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:BER

.. code-block:: python

	READ:GSM:MEASurement<Instance>:MEValuation:BER
	FETCh:GSM:MEASurement<Instance>:MEValuation:BER



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Ber.Ber
	:members:
	:undoc-members:
	:noindex: