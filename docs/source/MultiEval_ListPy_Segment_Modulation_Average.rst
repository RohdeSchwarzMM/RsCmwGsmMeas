Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: