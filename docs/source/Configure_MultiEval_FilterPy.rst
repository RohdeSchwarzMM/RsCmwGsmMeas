FilterPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:FILTer:PVTime
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:FILTer:IQ

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:FILTer:PVTime
	CONFigure:GSM:MEASurement<Instance>:MEValuation:FILTer:IQ



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.FilterPy.FilterPy
	:members:
	:undoc-members:
	:noindex: