Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.Modulation_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: