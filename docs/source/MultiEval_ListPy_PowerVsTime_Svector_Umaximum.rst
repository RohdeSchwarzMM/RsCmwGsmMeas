Umaximum
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Umaximum.Umaximum
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.powerVsTime.svector.umaximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_PowerVsTime_Svector_Umaximum_Current.rst
	MultiEval_ListPy_PowerVsTime_Svector_Umaximum_Average.rst
	MultiEval_ListPy_PowerVsTime_Svector_Umaximum_Minimum.rst
	MultiEval_ListPy_PowerVsTime_Svector_Umaximum_Maximum.rst