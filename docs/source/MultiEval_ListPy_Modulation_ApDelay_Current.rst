Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:APDelay:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:APDelay:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:APDelay:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:APDelay:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.ApDelay_.Current.Current
	:members:
	:undoc-members:
	:noindex: