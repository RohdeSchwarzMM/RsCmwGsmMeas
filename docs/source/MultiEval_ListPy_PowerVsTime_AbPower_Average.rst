Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.AbPower_.Average.Average
	:members:
	:undoc-members:
	:noindex: