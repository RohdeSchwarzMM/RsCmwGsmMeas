from typing import List

from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal import Conversions
from .....Internal.ArgSingleSuppressed import ArgSingleSuppressed
from .....Internal.Types import DataType


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Count:
	"""Count commands group definition. 1 total commands, 0 Sub-groups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("count", core, parent)

	def fetch(self) -> List[int or bool]:
		"""SCPI: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:COUNt \n
		Snippet: value: List[int or bool] = driver.multiEval.listPy.ber.count.fetch() \n
		Returns the number of measured bursts for each list mode segment. \n
		Use RsCmwGsmMeas.reliability.last_value to read the updated reliability indicator. \n
			:return: ber_count: Comma-separated list of values, one per measured segment Range: 0 to StatisticCount"""
		suppressed = ArgSingleSuppressed(0, DataType.Integer, False, 1, 'Reliability')
		response = self._core.io.query_str_suppressed(f'FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:COUNt?', suppressed)
		return Conversions.str_to_int_or_bool_list(response)
