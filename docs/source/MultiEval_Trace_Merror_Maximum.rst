Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Merror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: