Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: