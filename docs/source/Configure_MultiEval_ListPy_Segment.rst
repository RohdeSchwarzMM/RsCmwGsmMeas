Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr512
	rc = driver.configure.multiEval.listPy.segment.repcap_segment_get()
	driver.configure.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment.Segment
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Setup.rst
	Configure_MultiEval_ListPy_Segment_Modulation.rst
	Configure_MultiEval_ListPy_Segment_PowerVsTime.rst
	Configure_MultiEval_ListPy_Segment_Smodulation.rst
	Configure_MultiEval_ListPy_Segment_Sswitching.rst
	Configure_MultiEval_ListPy_Segment_Ber.rst
	Configure_MultiEval_ListPy_Segment_SingleCmw.rst