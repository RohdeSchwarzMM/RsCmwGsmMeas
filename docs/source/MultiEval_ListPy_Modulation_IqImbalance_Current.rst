Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Current.Current
	:members:
	:undoc-members:
	:noindex: