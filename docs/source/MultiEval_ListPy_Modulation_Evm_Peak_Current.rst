Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: