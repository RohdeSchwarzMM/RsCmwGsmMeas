Modulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:MODulation:DECode

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:MODulation:DECode



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex: