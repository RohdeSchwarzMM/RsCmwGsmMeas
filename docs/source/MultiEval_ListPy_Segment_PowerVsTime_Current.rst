Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.PowerVsTime_.Current.Current
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.powerVsTime.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_PowerVsTime_Current_Svector.rst