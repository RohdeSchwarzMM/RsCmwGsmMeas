Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.PowerVsTime_.Average.Average
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.powerVsTime.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_PowerVsTime_Average_Svector.rst