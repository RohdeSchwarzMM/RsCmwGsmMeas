MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:GSM:MEASurement<Instance>:MEValuation
	single: STOP:GSM:MEASurement<Instance>:MEValuation
	single: ABORt:GSM:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:GSM:MEASurement<Instance>:MEValuation
	STOP:GSM:MEASurement<Instance>:MEValuation
	ABORt:GSM:MEASurement<Instance>:MEValuation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_State.rst
	MultiEval_Trace.rst
	MultiEval_MvThroughput.rst
	MultiEval_PowerVsTime.rst
	MultiEval_Modulation.rst
	MultiEval_Sswitching.rst
	MultiEval_Smodulation.rst
	MultiEval_Ber.rst
	MultiEval_ListPy.rst