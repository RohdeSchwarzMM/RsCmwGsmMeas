Average
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Average.Average
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Average_Svector.rst