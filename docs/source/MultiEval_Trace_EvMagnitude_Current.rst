Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Current.Current
	:members:
	:undoc-members:
	:noindex: