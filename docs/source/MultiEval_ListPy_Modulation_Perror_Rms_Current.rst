Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: