Qam<QamOrder>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr16 .. Nr16
	rc = driver.configure.multiEval.limit.qam.repcap_qamOrder_get()
	driver.configure.multiEval.limit.qam.repcap_qamOrder_set(repcap.QamOrder.Nr16)





.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam.Qam
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.qam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Qam_PowerVsTime.rst
	Configure_MultiEval_Limit_Qam_EvMagnitude.rst
	Configure_MultiEval_Limit_Qam_Merror.rst
	Configure_MultiEval_Limit_Qam_Perror.rst
	Configure_MultiEval_Limit_Qam_IqOffset.rst
	Configure_MultiEval_Limit_Qam_IqImbalance.rst
	Configure_MultiEval_Limit_Qam_Terror.rst
	Configure_MultiEval_Limit_Qam_FreqError.rst
	Configure_MultiEval_Limit_Qam_Smodulation.rst
	Configure_MultiEval_Limit_Qam_Sswitching.rst