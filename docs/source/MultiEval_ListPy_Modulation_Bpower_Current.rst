Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Bpower_.Current.Current
	:members:
	:undoc-members:
	:noindex: