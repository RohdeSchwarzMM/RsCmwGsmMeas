MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RPMode
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:FCRange
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:HDALevel
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:MSLots
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:TSEQuence
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:NBQSearch
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:ABSearch
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:MVIew
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:AMODe
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:APATtern
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:GLENgth
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:PCLMode
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:PCL
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:IIOFrames

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:GSM:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:GSM:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RPMode
	CONFigure:GSM:MEASurement<Instance>:MEValuation:FCRange
	CONFigure:GSM:MEASurement<Instance>:MEValuation:HDALevel
	CONFigure:GSM:MEASurement<Instance>:MEValuation:MSLots
	CONFigure:GSM:MEASurement<Instance>:MEValuation:TSEQuence
	CONFigure:GSM:MEASurement<Instance>:MEValuation:NBQSearch
	CONFigure:GSM:MEASurement<Instance>:MEValuation:ABSearch
	CONFigure:GSM:MEASurement<Instance>:MEValuation:MVIew
	CONFigure:GSM:MEASurement<Instance>:MEValuation:AMODe
	CONFigure:GSM:MEASurement<Instance>:MEValuation:APATtern
	CONFigure:GSM:MEASurement<Instance>:MEValuation:GLENgth
	CONFigure:GSM:MEASurement<Instance>:MEValuation:PCLMode
	CONFigure:GSM:MEASurement<Instance>:MEValuation:PCL
	CONFigure:GSM:MEASurement<Instance>:MEValuation:IIOFrames



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy.rst
	Configure_MultiEval_Vamos.rst
	Configure_MultiEval_FilterPy.rst
	Configure_MultiEval_Rotation.rst
	Configure_MultiEval_Modulation.rst
	Configure_MultiEval_Scount.rst
	Configure_MultiEval_Result.rst
	Configure_MultiEval_Limit.rst
	Configure_MultiEval_Smodulation.rst
	Configure_MultiEval_Sswitching.rst
	Configure_MultiEval_Ber.rst