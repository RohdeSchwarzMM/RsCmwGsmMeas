SingleCmw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.SingleCmw.SingleCmw
	:members:
	:undoc-members:
	:noindex: