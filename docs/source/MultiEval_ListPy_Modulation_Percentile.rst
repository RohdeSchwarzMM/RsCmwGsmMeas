Percentile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERCentile
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERCentile

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERCentile
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERCentile



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Percentile.Percentile
	:members:
	:undoc-members:
	:noindex: