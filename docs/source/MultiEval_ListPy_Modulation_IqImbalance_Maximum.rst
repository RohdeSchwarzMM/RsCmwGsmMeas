Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: