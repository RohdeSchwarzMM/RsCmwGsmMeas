PowerVsTime
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:PVTime



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.PowerVsTime.PowerVsTime
	:members:
	:undoc-members:
	:noindex: