Sswitching
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SSWitching:OFRequence
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SSWitching:TDFSelect
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SSWitching:PHMode

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:SSWitching:OFRequence
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SSWitching:TDFSelect
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SSWitching:PHMode



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Sswitching.Sswitching
	:members:
	:undoc-members:
	:noindex: