Mpoint<MeasPoint>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr20
	rc = driver.configure.multiEval.limit.qam.smodulation.mpoint.repcap_measPoint_get()
	driver.configure.multiEval.limit.qam.smodulation.mpoint.repcap_measPoint_set(repcap.MeasPoint.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:SMODulation:MPOint<MeasPoint>

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:SMODulation:MPOint<MeasPoint>



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.Smodulation_.Mpoint.Mpoint
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.qam.smodulation.mpoint.clone()