Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Terror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: