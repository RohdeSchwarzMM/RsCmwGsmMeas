Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: