Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:BER

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:BER



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Ber.Ber
	:members:
	:undoc-members:
	:noindex: