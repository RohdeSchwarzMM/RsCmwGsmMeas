EvMagnitude
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:EVMagnitude

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:EVMagnitude



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.EvMagnitude.EvMagnitude
	:members:
	:undoc-members:
	:noindex: