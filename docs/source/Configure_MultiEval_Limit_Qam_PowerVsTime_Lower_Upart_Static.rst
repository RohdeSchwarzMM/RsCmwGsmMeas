Static
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:LOWer:UPARt<UsefulPart>:STATic

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:LOWer:UPARt<UsefulPart>:STATic



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.PowerVsTime_.Lower_.Upart_.Static.Static
	:members:
	:undoc-members:
	:noindex: