Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:MEASurement<Instance>:SCENario:CSPath
	single: ROUTe:GSM:MEASurement<Instance>:SCENario:SALone
	single: ROUTe:GSM:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:GSM:MEASurement<Instance>:SCENario:CSPath
	ROUTe:GSM:MEASurement<Instance>:SCENario:SALone
	ROUTe:GSM:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwGsmMeas.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_MaProtocol.rst