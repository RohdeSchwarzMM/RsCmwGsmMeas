Absolute
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:ABSolute

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:ABSolute



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Ber_.Absolute.Absolute
	:members:
	:undoc-members:
	:noindex: