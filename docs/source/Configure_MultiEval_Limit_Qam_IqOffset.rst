IqOffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:IQOFfset

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:IQOFfset



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.IqOffset.IqOffset
	:members:
	:undoc-members:
	:noindex: