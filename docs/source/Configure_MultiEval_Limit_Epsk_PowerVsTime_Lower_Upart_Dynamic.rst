Dynamic<RangePcl>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.multiEval.limit.epsk.powerVsTime.lower.upart.dynamic.repcap_rangePcl_get()
	driver.configure.multiEval.limit.epsk.powerVsTime.lower.upart.dynamic.repcap_rangePcl_set(repcap.RangePcl.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:PVTime:LOWer:UPARt<UsefulPart>:DYNamic<RangePcl>

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:PVTime:LOWer:UPARt<UsefulPart>:DYNamic<RangePcl>



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Epsk_.PowerVsTime_.Lower_.Upart_.Dynamic.Dynamic
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.epsk.powerVsTime.lower.upart.dynamic.clone()