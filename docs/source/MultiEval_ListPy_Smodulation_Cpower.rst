Cpower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:CPOWer
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:CPOWer

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:CPOWer
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:CPOWer



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Smodulation_.Cpower.Cpower
	:members:
	:undoc-members:
	:noindex: