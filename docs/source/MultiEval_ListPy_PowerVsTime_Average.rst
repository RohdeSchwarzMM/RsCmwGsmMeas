Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Average.Average
	:members:
	:undoc-members:
	:noindex: