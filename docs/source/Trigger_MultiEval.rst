MultiEval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:GSM:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:GSM:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:GSM:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:GSM:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:GSM:MEASurement<Instance>:MEValuation:MGAP

.. code-block:: python

	TRIGger:GSM:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:GSM:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:GSM:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:GSM:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:GSM:MEASurement<Instance>:MEValuation:MGAP



.. autoclass:: RsCmwGsmMeas.Implementations.Trigger_.MultiEval.MultiEval
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_MultiEval_Catalog.rst
	Trigger_MultiEval_ListPy.rst