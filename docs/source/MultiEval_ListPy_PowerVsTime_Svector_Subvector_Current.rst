Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:SUBVector<SubVector>:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:SUBVector<SubVector>:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Subvector_.Current.Current
	:members:
	:undoc-members:
	:noindex: