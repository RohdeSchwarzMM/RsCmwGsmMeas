Connector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:CMWS:CONNector



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.SingleCmw_.Connector.Connector
	:members:
	:undoc-members:
	:noindex: