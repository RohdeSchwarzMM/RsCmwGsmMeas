Smodulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:SMODulation:RPOWer

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:SMODulation:RPOWer



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Epsk_.Smodulation.Smodulation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.epsk.smodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Epsk_Smodulation_Mpoint.rst