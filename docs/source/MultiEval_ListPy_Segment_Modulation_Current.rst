Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.Modulation_.Current.Current
	:members:
	:undoc-members:
	:noindex: