Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.PowerVsTime_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: