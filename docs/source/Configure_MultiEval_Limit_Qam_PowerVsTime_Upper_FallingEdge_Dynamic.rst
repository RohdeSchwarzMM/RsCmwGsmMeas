Dynamic<RangePcl>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.multiEval.limit.qam.powerVsTime.upper.fallingEdge.dynamic.repcap_rangePcl_get()
	driver.configure.multiEval.limit.qam.powerVsTime.upper.fallingEdge.dynamic.repcap_rangePcl_set(repcap.RangePcl.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:UPPer:FEDGe<FallingEdge>:DYNamic<RangePcl>

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:UPPer:FEDGe<FallingEdge>:DYNamic<RangePcl>



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.PowerVsTime_.Upper_.FallingEdge_.Dynamic.Dynamic
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.qam.powerVsTime.upper.fallingEdge.dynamic.clone()