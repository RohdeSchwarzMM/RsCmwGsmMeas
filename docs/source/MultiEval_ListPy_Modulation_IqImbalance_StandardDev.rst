StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:SDEViation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: