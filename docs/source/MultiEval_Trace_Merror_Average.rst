Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Merror_.Average.Average
	:members:
	:undoc-members:
	:noindex: