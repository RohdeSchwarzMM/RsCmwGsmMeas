Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PEAK:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Peak_.Average.Average
	:members:
	:undoc-members:
	:noindex: