Upart<UsefulPart>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.multiEval.limit.qam.powerVsTime.lower.upart.repcap_usefulPart_get()
	driver.configure.multiEval.limit.qam.powerVsTime.lower.upart.repcap_usefulPart_set(repcap.UsefulPart.Nr1)





.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.PowerVsTime_.Lower_.Upart.Upart
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.qam.powerVsTime.lower.upart.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Qam_PowerVsTime_Lower_Upart_Static.rst
	Configure_MultiEval_Limit_Qam_PowerVsTime_Lower_Upart_Dynamic.rst