Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQIMbalance:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqImbalance_.Average.Average
	:members:
	:undoc-members:
	:noindex: