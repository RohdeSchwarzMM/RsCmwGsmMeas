StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:SDEViation

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:SDEViation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Peak_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: