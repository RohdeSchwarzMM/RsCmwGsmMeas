from typing import List

from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal.ArgSingleSuppressed import ArgSingleSuppressed
from ....Internal.Types import DataType


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class Sreliability:
	"""Sreliability commands group definition. 1 total commands, 0 Sub-groups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._base = CommandsGroup("sreliability", core, parent)

	def fetch(self) -> List[int]:
		"""SCPI: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SRELiability \n
		Snippet: value: List[int] = driver.multiEval.listPy.sreliability.fetch() \n
		Returns the segment reliability for all measured list mode segments. A common reliability indicator of zero indicates
		that the results in all measured segments are valid. A non-zero value indicates that an error occurred in at least one of
		the measured segments. If you get a non-zero common reliability indicator, you can use this command to retrieve the
		individual reliability values of all measured segments for further analysis. \n
		Use RsCmwGsmMeas.reliability.last_value to read the updated reliability indicator. \n
			:return: seg_reliability: decimal Comma-separated list of values, one per measured segment The meaning of the returned values is the same as for the common reliability indicator, see previous parameter."""
		suppressed = ArgSingleSuppressed(0, DataType.Integer, False, 1, 'Reliability')
		response = self._core.io.query_bin_or_ascii_int_list_suppressed(f'FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SRELiability?', suppressed)
		return response
