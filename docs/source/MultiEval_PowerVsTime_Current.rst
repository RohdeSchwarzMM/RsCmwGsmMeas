Current
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Current.Current
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.powerVsTime.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_PowerVsTime_Current_Svector.rst