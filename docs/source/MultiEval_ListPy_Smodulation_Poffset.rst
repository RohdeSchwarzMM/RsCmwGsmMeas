Poffset<FreqOffset>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr41
	rc = driver.multiEval.listPy.smodulation.poffset.repcap_freqOffset_get()
	driver.multiEval.listPy.smodulation.poffset.repcap_freqOffset_set(repcap.FreqOffset.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:POFFset<FreqOffset>
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:POFFset<FreqOffset>

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:POFFset<FreqOffset>
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SMODulation:POFFset<FreqOffset>



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Smodulation_.Poffset.Poffset
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.smodulation.poffset.clone()