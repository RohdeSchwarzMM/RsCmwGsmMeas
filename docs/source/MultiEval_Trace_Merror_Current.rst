Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:MERRor:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Merror_.Current.Current
	:members:
	:undoc-members:
	:noindex: