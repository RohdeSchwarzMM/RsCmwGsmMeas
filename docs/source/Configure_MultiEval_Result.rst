Result
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:ALL
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:PVTime
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SMFRequency
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SMTime
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SSFRequency
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SSTime
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:AMPM
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:MSCalar
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:BER
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:IQ

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:ALL
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:PVTime
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:EVMagnitude
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SMFRequency
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SMTime
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SSFRequency
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:SSTime
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:AMPM
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:MSCalar
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:BER
	CONFigure:GSM:MEASurement<Instance>:MEValuation:RESult:IQ



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Result.Result
	:members:
	:undoc-members:
	:noindex: