Cpower
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:CPOWer
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:CPOWer

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:CPOWer
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:CPOWer



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Sswitching_.Cpower.Cpower
	:members:
	:undoc-members:
	:noindex: