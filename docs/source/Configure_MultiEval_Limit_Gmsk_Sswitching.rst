Sswitching
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:SSWitching:PLEVel

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:SSWitching:PLEVel



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Gmsk_.Sswitching.Sswitching
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.gmsk.sswitching.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Gmsk_Sswitching_Mpoint.rst