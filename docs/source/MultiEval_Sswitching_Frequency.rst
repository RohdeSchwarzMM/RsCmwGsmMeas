Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency
	single: READ:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency

.. code-block:: python

	CALCulate:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency
	FETCh:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency
	READ:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Sswitching_.Frequency.Frequency
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sswitching.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sswitching_Frequency_Limits.rst