Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.PowerVsTime_.Current.Current
	:members:
	:undoc-members:
	:noindex: