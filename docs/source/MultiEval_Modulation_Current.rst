Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:CURRent
	single: READ:GSM:MEASurement<Instance>:MEValuation:MODulation:CURRent

.. code-block:: python

	CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:CURRent
	FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:CURRent
	READ:GSM:MEASurement<Instance>:MEValuation:MODulation:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Modulation_.Current.Current
	:members:
	:undoc-members:
	:noindex: