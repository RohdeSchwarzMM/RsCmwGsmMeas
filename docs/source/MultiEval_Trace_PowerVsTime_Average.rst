Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.PowerVsTime_.Average.Average
	:members:
	:undoc-members:
	:noindex: