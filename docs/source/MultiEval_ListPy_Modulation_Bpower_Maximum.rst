Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Bpower_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: