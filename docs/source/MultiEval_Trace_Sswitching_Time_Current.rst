Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:SSWitching:TIME:CURRent
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:SSWitching:TIME:CURRent

.. code-block:: python

	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:SSWitching:TIME:CURRent
	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:SSWitching:TIME:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Sswitching_.Time_.Current.Current
	:members:
	:undoc-members:
	:noindex: