Scount
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:PVTime
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:SMODulation
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:SSWitching
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:BER

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:PVTime
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:SMODulation
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:SSWitching
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SCOunt:BER



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Scount.Scount
	:members:
	:undoc-members:
	:noindex: