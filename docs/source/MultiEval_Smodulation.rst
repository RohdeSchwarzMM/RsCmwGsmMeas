Smodulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:SMODulation
	single: READ:GSM:MEASurement<Instance>:MEValuation:SMODulation

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:SMODulation
	READ:GSM:MEASurement<Instance>:MEValuation:SMODulation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Smodulation.Smodulation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.smodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Smodulation_Frequency.rst