Percentile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PERCentile
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PERCentile

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PERCentile
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PERCentile



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Percentile.Percentile
	:members:
	:undoc-members:
	:noindex: