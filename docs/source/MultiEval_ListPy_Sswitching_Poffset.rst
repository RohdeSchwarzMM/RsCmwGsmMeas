Poffset<FreqOffset>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr41
	rc = driver.multiEval.listPy.sswitching.poffset.repcap_freqOffset_get()
	driver.multiEval.listPy.sswitching.poffset.repcap_freqOffset_set(repcap.FreqOffset.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:POFFset<FreqOffset>
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:POFFset<FreqOffset>

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:POFFset<FreqOffset>
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SSWitching:POFFset<FreqOffset>



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Sswitching_.Poffset.Poffset
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.sswitching.poffset.clone()