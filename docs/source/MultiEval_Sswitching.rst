Sswitching
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:SSWitching
	single: READ:GSM:MEASurement<Instance>:MEValuation:SSWitching

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:SSWitching
	READ:GSM:MEASurement<Instance>:MEValuation:SSWitching



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Sswitching.Sswitching
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.sswitching.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Sswitching_Frequency.rst