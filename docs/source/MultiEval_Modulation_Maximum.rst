Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	single: READ:GSM:MEASurement<Instance>:MEValuation:MODulation:MAXimum

.. code-block:: python

	CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:MAXimum
	READ:GSM:MEASurement<Instance>:MEValuation:MODulation:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Modulation_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: