Subvector<SubVector>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.multiEval.listPy.powerVsTime.svector.subvector.repcap_subVector_get()
	driver.multiEval.listPy.powerVsTime.svector.subvector.repcap_subVector_set(repcap.SubVector.Nr1)





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Subvector.Subvector
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.powerVsTime.svector.subvector.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_PowerVsTime_Svector_Subvector_Current.rst
	MultiEval_ListPy_PowerVsTime_Svector_Subvector_Average.rst
	MultiEval_ListPy_PowerVsTime_Svector_Subvector_Minimum.rst
	MultiEval_ListPy_PowerVsTime_Svector_Subvector_Maximum.rst