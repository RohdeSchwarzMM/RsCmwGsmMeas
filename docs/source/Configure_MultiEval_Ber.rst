Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:BER:LOOP
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:BER:TSTart
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:BER:TRUN

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:BER:LOOP
	CONFigure:GSM:MEASurement<Instance>:MEValuation:BER:TSTart
	CONFigure:GSM:MEASurement<Instance>:MEValuation:BER:TRUN



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Ber.Ber
	:members:
	:undoc-members:
	:noindex: