Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:SMODulation:TIME:CURRent
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:SMODulation:TIME:CURRent

.. code-block:: python

	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:SMODulation:TIME:CURRent
	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:SMODulation:TIME:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Smodulation_.Time_.Current.Current
	:members:
	:undoc-members:
	:noindex: