Enums
=========

AcquisitionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcquisitionMode.GAP
	# All values (2x):
	GAP | PATTern

Band
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Band.G04
	# All values (6x):
	G04 | G085 | G09 | G18 | G19 | GG08

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

Decode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Decode.GTBits
	# All values (2x):
	GTBits | STANdard

FilterIq
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterIq.F90Khz
	# All values (3x):
	F90Khz | ISIRemoved | UNFiltered

FilterPvTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterPvTime.G05M
	# All values (2x):
	G05M | G10M

ListMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListMode.ONCE
	# All values (2x):
	ONCE | SEGMent

LoopType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoopType.C
	# All values (2x):
	C | SRB

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

PclMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PclMode.AUTO
	# All values (3x):
	AUTO | PCL | SIGNaling

PeakHoldMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeakHoldMode.PHOL
	# All values (2x):
	PHOL | SCO

RangeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RangeMode.NORMal
	# All values (2x):
	NORMal | WIDE

RefPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RefPowerMode.AVERage
	# All values (3x):
	AVERage | CURRent | DCOMpensated

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerFlag.IFPower
	# All values (3x):
	IFPower | OFF | ON

RfConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RfConverter.IRX1
	# Last value:
	value = enums.RfConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

Rotation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rotation.P38
	# All values (2x):
	P38 | P38R

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.CSPath
	# All values (4x):
	CSPath | MAPRotocol | NAV | SALone

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

SlotA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlotA.ACCess
	# All values (6x):
	ACCess | ANY | EPSK | GMSK | OFF | Q16

SlotB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlotB.EPSK
	# All values (3x):
	EPSK | GMSK | OFF

SlotInfo
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlotInfo.ACCess
	# All values (5x):
	ACCess | EPSK | GMSK | OFF | Q16

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

TscA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TscA.DUMM
	# Last value:
	value = enums.TscA.TSCA
	# All values (11x):
	DUMM | OFF | TSC0 | TSC1 | TSC2 | TSC3 | TSC4 | TSC5
	TSC6 | TSC7 | TSCA

TscB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TscB.AB0
	# Last value:
	value = enums.TscB.OFF
	# All values (18x):
	AB0 | AB1 | AB2 | AB3 | AB4 | AB5 | AB6 | AB7
	DUMMy | NB0 | NB1 | NB2 | NB3 | NB4 | NB5 | NB6
	NB7 | OFF

