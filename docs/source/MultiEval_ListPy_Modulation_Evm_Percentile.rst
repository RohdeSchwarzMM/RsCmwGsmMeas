Percentile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PERCentile
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PERCentile

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PERCentile
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:PERCentile



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Percentile.Percentile
	:members:
	:undoc-members:
	:noindex: