Maximum
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.PowerVsTime_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.powerVsTime.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_PowerVsTime_Maximum_Svector.rst