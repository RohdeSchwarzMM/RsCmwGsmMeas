Epsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:EVMagnitude
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:MERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:PERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:IQOFfset
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:IQIMbalance
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:TERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:FERRor

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:EVMagnitude
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:MERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:PERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:IQOFfset
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:IQIMbalance
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:TERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:EPSK:FERRor



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Epsk.Epsk
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.epsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Epsk_PowerVsTime.rst
	Configure_MultiEval_Limit_Epsk_Smodulation.rst
	Configure_MultiEval_Limit_Epsk_Sswitching.rst