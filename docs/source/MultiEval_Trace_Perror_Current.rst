Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Perror_.Current.Current
	:members:
	:undoc-members:
	:noindex: