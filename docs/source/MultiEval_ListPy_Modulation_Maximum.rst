Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: