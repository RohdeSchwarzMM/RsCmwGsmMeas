StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	single: READ:GSM:MEASurement<Instance>:MEValuation:MODulation:SDEViation

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:SDEViation
	READ:GSM:MEASurement<Instance>:MEValuation:MODulation:SDEViation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Modulation_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: