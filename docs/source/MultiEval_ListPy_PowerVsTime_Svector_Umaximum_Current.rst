Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:UMAXimum:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:UMAXimum:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Umaximum_.Current.Current
	:members:
	:undoc-members:
	:noindex: