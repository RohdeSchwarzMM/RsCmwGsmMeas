Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency
	single: READ:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency

.. code-block:: python

	CALCulate:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency
	FETCh:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency
	READ:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Smodulation_.Frequency.Frequency
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.smodulation.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Smodulation_Frequency_Limits.rst