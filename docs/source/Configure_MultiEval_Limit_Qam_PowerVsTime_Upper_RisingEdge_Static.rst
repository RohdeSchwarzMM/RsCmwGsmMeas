Static
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:UPPer:REDGe<RisingEdge>:STATic

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:UPPer:REDGe<RisingEdge>:STATic



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.PowerVsTime_.Upper_.RisingEdge_.Static.Static
	:members:
	:undoc-members:
	:noindex: