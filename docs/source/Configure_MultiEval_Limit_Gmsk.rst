Gmsk
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:EVMagnitude
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:MERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:PERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:IQOFfset
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:IQIMbalance
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:TERRor
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:FERRor

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:EVMagnitude
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:MERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:PERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:IQOFfset
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:IQIMbalance
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:TERRor
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:FERRor



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Gmsk.Gmsk
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.gmsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Gmsk_PowerVsTime.rst
	Configure_MultiEval_Limit_Gmsk_Smodulation.rst
	Configure_MultiEval_Limit_Gmsk_Sswitching.rst