Merror
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:MERRor

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:MERRor



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.Merror.Merror
	:members:
	:undoc-members:
	:noindex: