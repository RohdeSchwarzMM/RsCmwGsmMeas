FallingEdge<FallingEdge>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.multiEval.limit.qam.powerVsTime.upper.fallingEdge.repcap_fallingEdge_get()
	driver.configure.multiEval.limit.qam.powerVsTime.upper.fallingEdge.repcap_fallingEdge_set(repcap.FallingEdge.Nr1)





.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.PowerVsTime_.Upper_.FallingEdge.FallingEdge
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.qam.powerVsTime.upper.fallingEdge.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Qam_PowerVsTime_Upper_FallingEdge_Static.rst
	Configure_MultiEval_Limit_Qam_PowerVsTime_Upper_FallingEdge_Dynamic.rst