StandardDev
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:SDEViation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.StandardDev.StandardDev
	:members:
	:undoc-members:
	:noindex: