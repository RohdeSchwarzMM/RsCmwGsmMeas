Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:BAND
	single: CONFigure:GSM:MEASurement<Instance>:CHANnel

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:BAND
	CONFigure:GSM:MEASurement<Instance>:CHANnel



.. autoclass:: RsCmwGsmMeas.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings.rst
	Configure_MultiEval.rst