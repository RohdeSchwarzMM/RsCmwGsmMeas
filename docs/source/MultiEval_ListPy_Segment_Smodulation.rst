Smodulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SMODulation
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SMODulation

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SMODulation
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SMODulation



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.Smodulation.Smodulation
	:members:
	:undoc-members:
	:noindex: