Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: