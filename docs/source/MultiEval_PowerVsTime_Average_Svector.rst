Svector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:AVERage:SVECtor

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:AVERage:SVECtor



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Average_.Svector.Svector
	:members:
	:undoc-members:
	:noindex: