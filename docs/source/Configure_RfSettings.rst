RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:RFSettings:EATTenuation
	single: CONFigure:GSM:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:GSM:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:GSM:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:GSM:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:GSM:MEASurement<Instance>:RFSettings:MLOFfset

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:RFSettings:EATTenuation
	CONFigure:GSM:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:GSM:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:GSM:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:GSM:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:GSM:MEASurement<Instance>:RFSettings:MLOFfset



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex: