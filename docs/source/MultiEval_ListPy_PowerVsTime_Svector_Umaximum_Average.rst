Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:UMAXimum:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:UMAXimum:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Umaximum_.Average.Average
	:members:
	:undoc-members:
	:noindex: