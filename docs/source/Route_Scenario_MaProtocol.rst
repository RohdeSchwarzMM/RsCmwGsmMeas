MaProtocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:GSM:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:GSM:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCmwGsmMeas.Implementations.Route_.Scenario_.MaProtocol.MaProtocol
	:members:
	:undoc-members:
	:noindex: