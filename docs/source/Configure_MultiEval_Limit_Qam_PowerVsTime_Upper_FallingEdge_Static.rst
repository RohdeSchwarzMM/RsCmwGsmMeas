Static
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:UPPer:FEDGe<FallingEdge>:STATic

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:QAM<QamOrder>:PVTime:UPPer:FEDGe<FallingEdge>:STATic



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Qam_.PowerVsTime_.Upper_.FallingEdge_.Static.Static
	:members:
	:undoc-members:
	:noindex: