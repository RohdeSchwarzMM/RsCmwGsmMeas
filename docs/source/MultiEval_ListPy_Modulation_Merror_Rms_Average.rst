Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: