Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PVTime:MINimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.PowerVsTime_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: