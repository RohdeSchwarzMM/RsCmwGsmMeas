Percentile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:PERCentile
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:PERCentile

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:PERCentile
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:MODulation:PERCentile



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.Modulation_.Percentile.Percentile
	:members:
	:undoc-members:
	:noindex: