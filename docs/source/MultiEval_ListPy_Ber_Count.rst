Count
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:COUNt

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:COUNt



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Ber_.Count.Count
	:members:
	:undoc-members:
	:noindex: