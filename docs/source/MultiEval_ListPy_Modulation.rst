Modulation
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation.Modulation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Evm.rst
	MultiEval_ListPy_Modulation_Merror.rst
	MultiEval_ListPy_Modulation_Perror.rst
	MultiEval_ListPy_Modulation_IqOffset.rst
	MultiEval_ListPy_Modulation_IqImbalance.rst
	MultiEval_ListPy_Modulation_FreqError.rst
	MultiEval_ListPy_Modulation_Terror.rst
	MultiEval_ListPy_Modulation_Bpower.rst
	MultiEval_ListPy_Modulation_ApDelay.rst
	MultiEval_ListPy_Modulation_Average.rst
	MultiEval_ListPy_Modulation_Current.rst
	MultiEval_ListPy_Modulation_Maximum.rst
	MultiEval_ListPy_Modulation_StandardDev.rst
	MultiEval_ListPy_Modulation_Percentile.rst