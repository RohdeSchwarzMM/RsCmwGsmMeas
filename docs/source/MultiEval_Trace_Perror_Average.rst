Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Perror_.Average.Average
	:members:
	:undoc-members:
	:noindex: