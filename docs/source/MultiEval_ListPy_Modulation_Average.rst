Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: