Minimum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:SUBVector<SubVector>:MINimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:SVECtor:SUBVector<SubVector>:MINimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.Svector_.Subvector_.Minimum.Minimum
	:members:
	:undoc-members:
	:noindex: