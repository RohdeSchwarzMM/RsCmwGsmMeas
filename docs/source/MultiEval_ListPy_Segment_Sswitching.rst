Sswitching
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SSWitching
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SSWitching

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SSWitching
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SSWitching



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Segment_.Sswitching.Sswitching
	:members:
	:undoc-members:
	:noindex: