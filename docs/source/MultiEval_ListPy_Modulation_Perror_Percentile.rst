Percentile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PERCentile
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PERCentile

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PERCentile
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PERCentile



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Percentile.Percentile
	:members:
	:undoc-members:
	:noindex: