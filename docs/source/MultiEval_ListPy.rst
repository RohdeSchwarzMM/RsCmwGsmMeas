ListPy
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Sreliability.rst
	MultiEval_ListPy_PowerVsTime.rst
	MultiEval_ListPy_Modulation.rst
	MultiEval_ListPy_Smodulation.rst
	MultiEval_ListPy_Sswitching.rst
	MultiEval_ListPy_Ber.rst
	MultiEval_ListPy_Overview.rst
	MultiEval_ListPy_Segment.rst