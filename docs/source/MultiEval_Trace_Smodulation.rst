Smodulation
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Smodulation.Smodulation
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.smodulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Smodulation_Time.rst