Upper
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Gmsk_.PowerVsTime_.Upper.Upper
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.gmsk.powerVsTime.upper.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Gmsk_PowerVsTime_Upper_RisingEdge.rst
	Configure_MultiEval_Limit_Gmsk_PowerVsTime_Upper_Upart.rst
	Configure_MultiEval_Limit_Gmsk_PowerVsTime_Upper_FallingEdge.rst