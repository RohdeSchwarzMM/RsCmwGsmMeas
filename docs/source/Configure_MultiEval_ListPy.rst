ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SLENgth
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:LRANge
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:OSINdex
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:IIFRames
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SLENgth
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:LRANge
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:OSINdex
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:IIFRames
	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment.rst
	Configure_MultiEval_ListPy_SingleCmw.rst