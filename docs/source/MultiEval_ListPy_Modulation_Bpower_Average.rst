Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:BPOWer:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Bpower_.Average.Average
	:members:
	:undoc-members:
	:noindex: