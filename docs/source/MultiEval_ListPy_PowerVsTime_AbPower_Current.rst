Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:PVTime:ABPower:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.PowerVsTime_.AbPower_.Current.Current
	:members:
	:undoc-members:
	:noindex: