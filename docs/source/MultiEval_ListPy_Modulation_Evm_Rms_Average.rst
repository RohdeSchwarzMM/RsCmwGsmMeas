Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:RMS:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Evm_.Rms_.Average.Average
	:members:
	:undoc-members:
	:noindex: