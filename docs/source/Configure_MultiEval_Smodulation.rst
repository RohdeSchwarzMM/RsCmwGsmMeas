Smodulation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SMODulation:OFRequence
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SMODulation:EARea
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:SMODulation:TDFSelect

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:SMODulation:OFRequence
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SMODulation:EARea
	CONFigure:GSM:MEASurement<Instance>:MEValuation:SMODulation:TDFSelect



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Smodulation.Smodulation
	:members:
	:undoc-members:
	:noindex: