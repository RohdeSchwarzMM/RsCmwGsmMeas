RisingEdge<RisingEdge>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.multiEval.limit.gmsk.powerVsTime.upper.risingEdge.repcap_risingEdge_get()
	driver.configure.multiEval.limit.gmsk.powerVsTime.upper.risingEdge.repcap_risingEdge_set(repcap.RisingEdge.Nr1)





.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Gmsk_.PowerVsTime_.Upper_.RisingEdge.RisingEdge
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.gmsk.powerVsTime.upper.risingEdge.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Gmsk_PowerVsTime_Upper_RisingEdge_Static.rst
	Configure_MultiEval_Limit_Gmsk_PowerVsTime_Upper_RisingEdge_Dynamic.rst