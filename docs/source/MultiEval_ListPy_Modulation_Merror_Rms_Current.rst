Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:RMS:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Rms_.Current.Current
	:members:
	:undoc-members:
	:noindex: