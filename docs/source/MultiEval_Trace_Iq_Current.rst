Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent

.. code-block:: python

	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent
	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:IQ:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Iq_.Current.Current
	:members:
	:undoc-members:
	:noindex: