Vamos
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:VAMos:TSCSet

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:VAMos:TSCSet



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Vamos.Vamos
	:members:
	:undoc-members:
	:noindex: