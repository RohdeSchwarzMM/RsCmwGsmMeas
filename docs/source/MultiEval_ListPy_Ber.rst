Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Ber_Ber.rst
	MultiEval_ListPy_Ber_Absolute.rst
	MultiEval_ListPy_Ber_Count.rst