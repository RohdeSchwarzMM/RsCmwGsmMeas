Svector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:MAXimum:SVECtor

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:MAXimum:SVECtor



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Maximum_.Svector.Svector
	:members:
	:undoc-members:
	:noindex: