Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:AVERage
	single: READ:GSM:MEASurement<Instance>:MEValuation:MODulation:AVERage

.. code-block:: python

	CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:AVERage
	FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:AVERage
	READ:GSM:MEASurement<Instance>:MEValuation:MODulation:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Modulation_.Average.Average
	:members:
	:undoc-members:
	:noindex: