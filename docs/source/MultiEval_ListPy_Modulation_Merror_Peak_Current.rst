Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.Current.Current
	:members:
	:undoc-members:
	:noindex: