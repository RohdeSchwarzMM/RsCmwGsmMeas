Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Average.Average
	:members:
	:undoc-members:
	:noindex: