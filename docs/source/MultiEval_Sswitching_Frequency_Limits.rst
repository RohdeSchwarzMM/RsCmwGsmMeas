Limits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency:LIMits
	single: READ:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency:LIMits

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency:LIMits
	READ:GSM:MEASurement<Instance>:MEValuation:SSWitching:FREQuency:LIMits



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Sswitching_.Frequency_.Limits.Limits
	:members:
	:undoc-members:
	:noindex: