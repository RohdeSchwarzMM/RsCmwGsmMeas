Average
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:AVERage
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:AVERage

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:AVERage
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:AVERage



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Merror_.Peak_.Average.Average
	:members:
	:undoc-members:
	:noindex: