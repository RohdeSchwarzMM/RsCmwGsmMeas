Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.IqOffset_.Current.Current
	:members:
	:undoc-members:
	:noindex: