Btype
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:BTYPe

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:BTYPe



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Btype.Btype
	:members:
	:undoc-members:
	:noindex: