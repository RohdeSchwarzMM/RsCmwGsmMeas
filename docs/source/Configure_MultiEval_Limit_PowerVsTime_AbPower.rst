AbPower<AbPower>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr10
	rc = driver.configure.multiEval.limit.powerVsTime.abPower.repcap_abPower_get()
	driver.configure.multiEval.limit.powerVsTime.abPower.repcap_abPower_set(repcap.AbPower.Nr1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:PVTime:ABPower<AbPower>

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:PVTime:ABPower<AbPower>



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.PowerVsTime_.AbPower.AbPower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.powerVsTime.abPower.clone()