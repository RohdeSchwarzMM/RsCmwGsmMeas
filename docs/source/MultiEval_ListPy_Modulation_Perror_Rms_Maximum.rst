Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Perror_.Rms_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: