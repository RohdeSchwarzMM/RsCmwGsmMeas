ListPy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: TRIGger:GSM:MEASurement<Instance>:MEValuation:LIST:MODE

.. code-block:: python

	TRIGger:GSM:MEASurement<Instance>:MEValuation:LIST:MODE



.. autoclass:: RsCmwGsmMeas.Implementations.Trigger_.MultiEval_.ListPy.ListPy
	:members:
	:undoc-members:
	:noindex: