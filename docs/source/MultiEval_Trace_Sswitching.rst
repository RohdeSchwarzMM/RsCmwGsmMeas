Sswitching
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Sswitching.Sswitching
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.sswitching.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Sswitching_Time.rst