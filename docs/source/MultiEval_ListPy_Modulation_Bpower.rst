Bpower
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Bpower.Bpower
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.bpower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Bpower_Current.rst
	MultiEval_ListPy_Modulation_Bpower_Average.rst
	MultiEval_ListPy_Modulation_Bpower_Maximum.rst
	MultiEval_ListPy_Modulation_Bpower_StandardDev.rst