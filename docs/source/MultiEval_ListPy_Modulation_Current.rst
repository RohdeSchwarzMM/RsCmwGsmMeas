Current
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:CURRent
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:CURRent

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:CURRent
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:CURRent



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.Current.Current
	:members:
	:undoc-members:
	:noindex: