ApDelay
----------------------------------------





.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.ApDelay.ApDelay
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.apDelay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_ApDelay_Current.rst
	MultiEval_ListPy_Modulation_ApDelay_Average.rst
	MultiEval_ListPy_Modulation_ApDelay_Maximum.rst
	MultiEval_ListPy_Modulation_ApDelay_StandardDev.rst