Setup
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIST:SEGMent<Segment>:SETup



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.ListPy_.Segment_.Setup.Setup
	:members:
	:undoc-members:
	:noindex: