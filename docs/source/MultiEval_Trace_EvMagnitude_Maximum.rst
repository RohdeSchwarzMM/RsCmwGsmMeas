Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:EVMagnitude:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.EvMagnitude_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: