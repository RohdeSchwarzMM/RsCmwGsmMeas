Svector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:CURRent:SVECtor

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:CURRent:SVECtor



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Current_.Svector.Svector
	:members:
	:undoc-members:
	:noindex: