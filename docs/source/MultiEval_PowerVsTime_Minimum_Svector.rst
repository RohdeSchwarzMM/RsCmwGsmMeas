Svector
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:MINimum:SVECtor

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:PVTime:MINimum:SVECtor



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.PowerVsTime_.Minimum_.Svector.Svector
	:members:
	:undoc-members:
	:noindex: