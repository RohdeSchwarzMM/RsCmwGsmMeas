Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum
	CALCulate:GSM:MEASurement<Instance>:MEValuation:LIST:MODulation:FERRor:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Modulation_.FreqError_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: