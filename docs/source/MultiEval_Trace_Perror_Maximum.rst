Maximum
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum
	single: READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum
	READ:GSM:MEASurement<Instance>:MEValuation:TRACe:PERRor:MAXimum



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Trace_.Perror_.Maximum.Maximum
	:members:
	:undoc-members:
	:noindex: