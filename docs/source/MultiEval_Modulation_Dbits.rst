Dbits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:DBITs

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:DBITs



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Modulation_.Dbits.Dbits
	:members:
	:undoc-members:
	:noindex: