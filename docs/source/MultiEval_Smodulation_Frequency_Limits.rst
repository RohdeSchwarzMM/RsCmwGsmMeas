Limits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency:LIMits
	single: READ:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency:LIMits

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency:LIMits
	READ:GSM:MEASurement<Instance>:MEValuation:SMODulation:FREQuency:LIMits



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Smodulation_.Frequency_.Limits.Limits
	:members:
	:undoc-members:
	:noindex: