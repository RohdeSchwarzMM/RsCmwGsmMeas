Static
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:PVTime:UPPer:UPARt<UsefulPart>:STATic

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:LIMit:GMSK:PVTime:UPPer:UPARt<UsefulPart>:STATic



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Limit_.Gmsk_.PowerVsTime_.Upper_.Upart_.Static.Static
	:members:
	:undoc-members:
	:noindex: