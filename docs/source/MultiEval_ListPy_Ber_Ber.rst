Ber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:BER

.. code-block:: python

	FETCh:GSM:MEASurement<Instance>:MEValuation:LIST:BER:BER



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.ListPy_.Ber_.Ber.Ber
	:members:
	:undoc-members:
	:noindex: