Percentile
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:PERCentile
	single: FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:PERCentile
	single: READ:GSM:MEASurement<Instance>:MEValuation:MODulation:PERCentile

.. code-block:: python

	CALCulate:GSM:MEASurement<Instance>:MEValuation:MODulation:PERCentile
	FETCh:GSM:MEASurement<Instance>:MEValuation:MODulation:PERCentile
	READ:GSM:MEASurement<Instance>:MEValuation:MODulation:PERCentile



.. autoclass:: RsCmwGsmMeas.Implementations.MultiEval_.Modulation_.Percentile.Percentile
	:members:
	:undoc-members:
	:noindex: