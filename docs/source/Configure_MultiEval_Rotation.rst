Rotation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:GSM:MEASurement<Instance>:MEValuation:ROTation:IQ

.. code-block:: python

	CONFigure:GSM:MEASurement<Instance>:MEValuation:ROTation:IQ



.. autoclass:: RsCmwGsmMeas.Implementations.Configure_.MultiEval_.Rotation.Rotation
	:members:
	:undoc-members:
	:noindex: